package com.jeesuite.bestpl.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jeesuite.bestpl.api.IPostService;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageMetaInfo;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;

@Controller
@RequestMapping("/")
public class IndexController {
	
	private @Autowired IPostService postService;
	
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(Model model){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("index"));
		
		//最新
		model.addAttribute("newPosts", postService.findTopPost(0, "new", 15));
		//推荐
		model.addAttribute("recPosts", postService.findTopPost(0, "recommend", 5));
		
		//分类
		model.addAttribute("categories", postService.findAllPostCategory());
		//热门tag
		model.addAttribute("hotTags", postService.findHotTags(50));
		
		return "index";
	}

	@RequestMapping(value = "category/{categoryId}/{pageNo}", method = RequestMethod.GET)
	public String languege(Model model,@PathVariable("categoryId") int languegeId,@PathVariable(value="pageNo") int pageNo){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("post"));
		PageQueryParam param = new PageQueryParam(pageNo);
		param.getConditions().put("languegeId", languegeId);
		Page<Post> Page = postService.pageQueryPost(param);
		model.addAttribute("page", Page);
		return "post/category";
	}
	
	@RequestMapping(value = "tag/{tag}/{pageNo}", method = RequestMethod.GET)
	public String tag(Model model,@PathVariable("tag") String tag,@PathVariable(value="pageNo") int pageNo){
		model.addAttribute(PageMetaInfo.ATTR_NAME, PageMetaInfo.current().setModelName("post"));
		PageQueryParam param = new PageQueryParam(pageNo);
		param.getConditions().put("tag", tag);
		Page<Post> Page = postService.pageQueryPost(param);
		model.addAttribute("page", Page);
		return "post/tag";
	}
	
	
}
