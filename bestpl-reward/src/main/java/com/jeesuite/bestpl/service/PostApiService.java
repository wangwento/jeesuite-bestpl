package com.jeesuite.bestpl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.jeesuite.bestpl.dto.Comment;
import com.jeesuite.bestpl.dto.IdNamePair;
import com.jeesuite.bestpl.dto.Page;
import com.jeesuite.bestpl.dto.PageQueryParam;
import com.jeesuite.bestpl.dto.Post;

@Service
public class PostApiService{
	
	@Value("${post.rest.basePath}")
	private String postRestBasePath;

	@Autowired
	private RestTemplate restTemplate;

	
	public List<IdNamePair> findAllPostCategory() {
		ParameterizedTypeReference<List<IdNamePair>> arearesponseType = new ParameterizedTypeReference<List<IdNamePair>>() {
		};
		List<IdNamePair> lists = restTemplate
				.exchange("http://CLOUD-COMMON-SERVICE/region/provinces", HttpMethod.GET, null, arearesponseType)
				.getBody();
		return lists;
	}

	
	public void addPosts(Post post) {
		// TODO Auto-generated method stub
		
	}

	
	public Post findPostById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public List<Post> findTopPost(int categoryId, String sortType, int top) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Page<Post> pageQueryPost(PageQueryParam param) {
		ParameterizedTypeReference<Page<Post>> arearesponseType = new ParameterizedTypeReference<Page<Post>>() {
		};
		HttpEntity<PageQueryParam> httpEntity = new HttpEntity<PageQueryParam>(param);
		Page<Post> page = restTemplate.exchange("http://CLOUD-COMMON-SERVICE/contactus/list",
				HttpMethod.POST, httpEntity, arearesponseType).getBody();
		return page;
	}

	
	public void addPostComment(Comment comment) {
		// TODO Auto-generated method stub
		
	}

	
	public Page<Comment> pageQueryPostComment(int postId, int pageNo, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
